##HOW TO USE

- Download [composer](https://getcomposer.org/download)
- Run `php composer.phar install` on your terminal or cmd prompt (windows) and wait for the vendors to be downloaded
- Change the database settings in /app/config/database.php -- default "mysql"
- Run `php artisan migrate --seed`
- Run `php artisan serve`
- Goto http://localhost:8000